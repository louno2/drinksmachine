export default () => async (ctx, next) => {
  try {
    await next();
  } catch ({ status = 500, message = 'Server Error', name, errors }) {
    if (name === 'ValidationError') {
      ctx.status = 400;
      ctx.body = {
        message: Object.values(errors)
          .map(({ message }) => message).join(', ')
      };
    } else {
      ctx.status = status;
      ctx.body = { status, message };
    }
  }
};
