import bodyParser from 'koa-bodyparser';
import logger from 'koa-logger';
import serve from 'koa-static';
import cors from 'koa2-cors';
import { IS_DEV } from '../utils/env';
import error from './error';
import apiRouter from '../modules';

import {
  SERVE_OPTIONS,
  DIST_DIR
} from '../config';

export default (app) => {

  if (IS_DEV) {
    app.use(logger());
  }

  app.use(cors());
  app.use(error());
  app.use(bodyParser());
  app.use(serve(DIST_DIR, SERVE_OPTIONS));
  app.use(apiRouter.routes());
  app.use(apiRouter.allowedMethods());

};
