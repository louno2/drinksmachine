import get from 'lodash/get';
import { SECRET_KEY } from '../config';

export default () => async (ctx, next) => {

  const { authorization } = ctx.headers;
  const { query } = ctx.request;

  const privacyType = get(ctx,
    'request.query.privacyType',
    'admin'
  );

  if (authorization !== SECRET_KEY && privacyType === 'admin') {
    ctx.throw(401, { message: 'Неверный секретный ключ' });
  }
  ctx.state.privacyType = privacyType;

  await next();
};
