import Router from 'koa-router';
import Coin from './model';
import coinController from './controllers/coin-controller';
import checkCoinByHash from './handlers/checkCoinByHash';
import checkSecretKey from '../../handlers/checkSecretKey';

const router = new Router({ prefix: '/api/coin' });

router
  .get('/',  coinController.list)
  .param('hash', checkCoinByHash())
  .put('/:hash', checkSecretKey(), coinController.update)

export {
  Coin
}

export default router.routes();
