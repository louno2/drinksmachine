import Coin from '../model';

export default {
  updateCoinData(coin, data) {
    coin.set(data);
    try {
      return coin.save();
    } catch (e) {
      throw new AppError({status: 400, ...e});
    }
  },

  searchCoins() {
    return Coin.find({}).lean();
  }
}
