import Coin from '../model';

export default () => async (hash, ctx, next) => {

  const coin = await Coin.findOne({ hash });

  if (!coin) {
    ctx.throw(404, `Coin с хешом "${hash}" не найден`);
  }

  ctx.state.coin = coin;

  await next();
}
