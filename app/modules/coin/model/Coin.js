import mongoose, { Schema } from 'mongoose';

const CoinSchema = new Schema({
  hash: {
    type: String,
    unique: 'значение hash должно быть уникальным'
  },
  title: {
    type: String,
    required: 'название не может быть пустым',
    unique: 'значение названия монеты должно быть уникальным'
  },
  faceValue: {
    type: Number,
    unique: 'значение номинала должны быть уникальными'
  },
  active: {
    type: Boolean,
    default: true
  }
}, {
  versionKey: false
});

CoinSchema.statics.Fields = [
  'hash', 'title',
  'faceValue', 'active'
]

export default mongoose.model('coin', CoinSchema);
