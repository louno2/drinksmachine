import pick from 'lodash/pick';
import { CoinService } from '../service';
import Coin from '../model';

export default {

  async update(ctx) {

    const {
      request: { body },
      state: { coin }
    } = ctx;

    const newData = pick(body, Coin.Fields);

    const updatedCoin = await CoinService
      .updateCoinData(coin, newData);

    ctx.body = {
      coin: pick(
        updatedCoin, Coin.Fields,
      ),
      message: 'Настройки монеты обновлены'
    }
  },

  async list(ctx) {
      let coins = await CoinService.searchCoins();
      ctx.body = {
        coins: coins.map(coin =>
            pick(coin, Coin.Fields)
          )
      };
  }
}
