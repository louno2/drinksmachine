export default {
    async getPagesByUserHash(ctx) {
        const { state: { user: { hash: userHash } } } = ctx;
        const pages = await Pages.find({ userHash });

        ctx.body = { data: pages };
    },
};
