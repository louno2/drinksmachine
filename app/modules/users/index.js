import Router from 'koa-router';
import checkUserByHash from './handlers/checkUserByHash';
import { User } from './models';
import UsersController from './controllers/users-controller';

const router = new Router({ prefix: '/api/users' });

router
    .param('hash', checkUserByHash())
    .get('/:hash/summaries', UsersController.getPagesByUserHash);

export {
    User,
};

export default router.routes();
