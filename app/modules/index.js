import fs from 'fs';
import Router from 'koa-router';

import coin from './coin';
import drink from './drink';

import { DIST_DIR } from '../config';
import { IS_PROD } from '../utils/env';

const router = new Router();
router.use(coin, drink);

const rootRouter = new Router();

if (IS_PROD) {

    rootRouter.get('*', async(ctx) => {
        ctx.type = 'html';
        ctx.body = fs.createReadStream(`${DIST_DIR}/index.html`);
    });

    router.use('/',
        rootRouter.routes(),
        rootRouter.allowedMethods()
    );
}

export default router
