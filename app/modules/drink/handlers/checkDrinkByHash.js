import Drink from '../model';

export default () => async (hash, ctx, next) => {

  const drink = await Drink.findOne({ hash });

  if (!drink) {
    ctx.throw(404, `Drink с хешом "${hash}" не найден`);
  }

  ctx.state.drink = drink;

  await next();
}
