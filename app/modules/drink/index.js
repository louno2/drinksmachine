import Router from 'koa-router';
import drinkController from './controllers/drink-controller';
import checkSecretKey from '../../handlers/checkSecretKey';
import checkDrinkByHash from './handlers/checkDrinkByHash'

const router = new Router({ prefix: '/api/drink' });

router
    .post('/', checkSecretKey(), drinkController.createOrUpdate)
    .post('/complete-order', drinkController.completeOrder)
    .get('/', checkSecretKey(),  drinkController.list)
    .param('hash', checkDrinkByHash())
    .delete('/:hash', checkSecretKey(), drinkController.remove);


export default router.routes();
