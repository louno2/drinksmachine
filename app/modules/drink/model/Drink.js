import mongoose, { Schema } from 'mongoose';

import uniqueValidator from 'mongoose-unique-validator';
mongoose.plugin(uniqueValidator);

const DrinkSchema = new Schema ({
    hash: {
        type: String,
        unique: 'значение hash должно быть уникальным'
    },
    title: {
        type: String,
        required: 'значение название не может быть пустым',
        unique: 'названия напитков должны быть уникальными'
    },
    cost: {
        type: Number,
        default: 0
    },
    quantity: {
        type: Number,
        default: 0
    }
}, {
    timestamps: true,
    versionKey: false
});

const commonFields = [
  'hash', 'title', 'cost', 'quantity'
];

DrinkSchema.statics.publicFields = commonFields;
DrinkSchema.statics.privacyFields = commonFields
  .concat(['createdAt']);

export default mongoose.model('drink', DrinkSchema);
