import pick from 'lodash/pick';
import { DrinkService } from '../service';
import Drink from '../model';

export default {

    async createOrUpdate(ctx) {

        const { body  } = ctx.request;
        const newData = pick(
          body, Drink.privacyFields
        );

        let updatedDrink = await DrinkService
            .createOrUpdateDrink(newData);

        ctx.status = 201;
        ctx.body = {
            drink: pick(updatedDrink,
                Drink.privacyFields
            ),
            message: 'Изменения по напиткам сохранены'
        };
    },

    async completeOrder(ctx) {
      const { body } = ctx.request;

      const [
        restAmount, drinksTitles
      ] = await DrinkService.completeOrder(body);

      ctx.body = {
        message: `Заказ выполнен, ${drinksTitles} ваша сдача (${body.amount} руб.) ${restAmount}`
      }
    },

    async list(ctx) {

      const { privacyType } = ctx.state;

      let drinks = await DrinkService.searchDrinks();
      ctx.body = {
        drinks: drinks.map(drink =>
          pick(drink,  Drink[
            privacyType === 'admin'
            ? 'privacyFields'
            : 'publicFields'
            ])
        )
      };
    },

    async remove(ctx) {

      const {
        state: { drink }
      } = ctx;

      await drink.remove();
      ctx.body = {
       message: 'Напиток удален'
      };
    }
}
