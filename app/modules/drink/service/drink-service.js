import { v4 as uuidv4 } from 'uuid';
import config from 'config';
import Drink from '../model';
const COINS_DATA = config.get('coins_data');

export default {
  createOrUpdateDrink ({hash, ...rest}) {

    if (!hash) {
      hash = uuidv4();
    }

    return Drink.findOneAndUpdate({ hash }, rest, {
      upsert: true, new: true,
      setDefaultsOnInsert: true
    }).lean()
  },

  async completeOrder({ orderedDrinks, amount }) {

    let listCoins = COINS_DATA.slice().sort(
      (prev, next) => next - prev
    );

    function calcValue(value, index, result) {

      let coin = listCoins[index];
      let rest = value % coin;
      value = value - rest;

      if (value) {
        result.push(`${coin} руб. монет - ${value / coin} шт.`);
      }

      if (rest === 0) {
        return result
      } else {
        return calcValue(rest, index + 1, result);
      }
    }

    let drinksTitles = '';
    let restAmount = calcValue(amount, 0, []);

    for (const drink of orderedDrinks) {
      drinksTitles = drinksTitles.concat(`${drink.title}, `);
      await Drink.findOneAndUpdate({ hash: drink.hash },
         {$inc: { quantity: -1 }}, {
          useFindAndModify: false
        }
      );
    }

    return [
      restAmount.join(','),
      drinksTitles
    ];
  },

  searchDrinks () {
    return Drink.find({}).lean()
  }
}
