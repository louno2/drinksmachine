import mongoose from 'mongoose';

mongoose.Promise = Promise;

export default (mongoUri) => {
    if (!mongoUri) {
        throw Error('Mongo uri is undefined');
    }

    return mongoose
        .connect(mongoUri,{
            useCreateIndex: true,
            useNewUrlParser: true,
            useUnifiedTopology: true })
        .then((mongodb) => {
            console.log('Mongo connected');

            return mongodb;
        });
};
