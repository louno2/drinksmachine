export default function evalStrMultiply(str){
    let total= 1;
    let matched = str.match(/[+\-]*(\.\d+|\d+(\.\d+)?)/g) || [];
    while(matched.length){
        total = total * parseFloat(matched.shift());
    }
    return total;
}
