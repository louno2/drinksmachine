import { v4 as uuidv4 } from 'uuid';
import config from 'config';
import { Coin } from '../modules/coin';
import mongooseConnector from '../connectors/mongoose-connector';

const MONGO_URI = config.get('mongo.uri');
const COINS_DATA = config.get('coins_data');

export default async () => {

  await mongooseConnector(MONGO_URI);
  try {
    if (COINS_DATA.length) {
      await Promise.all(COINS_DATA.map(
        item => Coin.create({
          hash: uuidv4(),
          title: `${item} руб.`,
          active: true,
          faceValue: item
        })
      ));
    }
  } catch (e) {
    console.error(e);
  } finally {
    console.log('all done');
    process.exit();
  }
};


