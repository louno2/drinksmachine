import config from 'config';
import path from 'path';
import evalStringMultiply from './utils/evalStringMultiply';

const PORT = process.env.PORT || config.get('port');
const MONGO_URI = config.get('mongo.uri');
const SECRET_KEY = config.get('secret_key');

const max_age = config.get('max_age');
const SERVE_OPTIONS = {
    maxage: evalStringMultiply(max_age)
};

const dist_path = config.get('dist_path');
const DIST_DIR = path.join(__dirname, dist_path);

export {
    PORT,
    MONGO_URI,
    DIST_DIR,
    SERVE_OPTIONS,
    SECRET_KEY
};
