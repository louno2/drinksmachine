import Koa from 'koa';
import connectorsInit from './connectors';
import initHandlers from './handlers';
import AppError from './helpers/appError';
connectorsInit();
global.AppError = AppError;

const app = new Koa();

initHandlers(app);

export default app;
