import index from './app';
import { PORT } from './app/config';

const server = index.listen(PORT, (err) => {
    if (err) console.error(err);

    console.log(`Server running on port: ${PORT}`);
});

export default server;
