import axios from 'axios';
import { handleSuccess, handleError } from '../utils/api';
const { API_URL ='' } = process.env;

export const postDrink = (data) =>
  axios.post(`${API_URL}/api/drink`, data)
    .then(handleSuccess)
    .catch(handleError);

export const getDrinks = (privacyType) =>
   axios.get(`${API_URL}/api/drink?privacyType=${privacyType}`)
     .then(handleSuccess)
     .catch(handleError);

export const completeOrder = (data) =>
  axios.post(`${API_URL}/api/drink/complete-order`, data)
    .then(handleSuccess)
    .catch(handleError);

export const deleteOrder = (hash) =>
  axios.delete(`${API_URL}/api/drink/${hash}`)
    .then(handleSuccess)
    .catch(handleError);

