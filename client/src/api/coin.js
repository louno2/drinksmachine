import axios from 'axios/index';
import { handleError, handleSuccess } from '../utils/api';
const { API_URL ='' } = process.env;

export const getCoins = () =>
  axios.get(`${API_URL}/api/coin`)
    .then(handleSuccess)
    .catch(handleError);

export const putCoin = (hash, data) =>
  axios.put(`${API_URL}/api/coin/${hash}`, data)
    .then(handleSuccess)
    .catch(handleError);
