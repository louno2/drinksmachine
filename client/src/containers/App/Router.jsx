import React from 'react';
import { Route, Switch } from 'react-router-dom';
import PublicPage from '../PublicPage';
import AdminPage from '../AdminPage';

const Router = () => (
    <div className="theme-light ltr-support">
        <div className="wrapper ltr-support">
        <main>
            <div className="container__wrap">
                <Switch>
                    <Route exact path="/" component={PublicPage}/>
                    <Route path="/admin" component={AdminPage}/>
                </Switch>
            </div>
        </main>
        </div>
    </div>
);

export default Router;
