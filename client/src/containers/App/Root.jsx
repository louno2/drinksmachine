import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';

import '../../scss/bootstrap.css';
import '../../scss/app.scss';

import Router from './Router';

export default function Root({ history, store }) {
    return (
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Router/>
            </ConnectedRouter>
        </Provider>
    );
}
