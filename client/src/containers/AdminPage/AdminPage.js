import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Container, Col, Row, Button } from 'reactstrap';

import DrinksList from '../DrinksList';
import ModifyDrink from './parts/ModifyDrink';
import ModifyCoins from './parts/ModifyCoins';

import { attemptGetDrinks } from '../../store/thunks/drink';
import { attemptGetCoins } from '../../store/thunks/coin';

import setAuthToken from '../../utils/setAuthToken';
import checkAuthError from '../../utils/checkAuthError';

export default function AdminPanel () {

  const dispatch = useDispatch();

  const [ loading, setLoading ] = useState(true);
  const [ isAuth, setIsAuth ] = useState(false);

  useEffect(() => {
    setAuthToken('secretKey', setIsAuth)
    return () => setAuthToken('');
  }, []);

  useEffect(() => {
    if (isAuth) {
      Promise.all([
        dispatch(attemptGetDrinks('admin')),
        dispatch(attemptGetCoins())
      ])
        .then(() => setLoading(false))
        .catch(checkAuthError(setIsAuth))
    }
  }, [isAuth])

  return !loading && isAuth && (
    <Container className="dashboard">
      <Link to="/">
        <Button>
          public
        </Button>
      </Link>
      <Row>
        <Col md={12}>
          <h3 className="page-title">Административная страница</h3>
        </Col>
      </Row>
      <Row>
        <ModifyDrink/>
        <ModifyCoins/>
      </Row>
      <Row>
        <DrinksList isPrivacy={true}/>
      </Row>
    </Container>
  );
}
