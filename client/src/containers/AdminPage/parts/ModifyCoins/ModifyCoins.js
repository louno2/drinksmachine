import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Col, Card, CardBody, Table,
  ButtonToolbar, Button
} from 'reactstrap';
import Alert from '../../../../shared/components/Alert';
import CheckBox from '../../../Common/CheckBox';

import { attemptToggleLockCoin } from '../../../../store/thunks/coin'

export default function ModifyCoins() {

  const dispatch = useDispatch();

  const {
    list: coins,
  } = useSelector(
    st => st.coin
  );

  const toggleCheckCoin = (hash, value) => {
    dispatch(attemptToggleLockCoin(hash, value));
  };

  return (
    <Col md={3}>
      <Card>
        <CardBody>
          <div className="card__title">
            <h5 className="bold-text">Настройка монет</h5>
          </div>
          { !coins.length ?
            <Alert color="info" className="alert--bordered" icon>
              <p> <span className="bold-text">Подсказка: </span>
                Не создано ни одной монеты.
              </p>
            </Alert>
           : <Table className="table--bordered">
              <thead>
              <tr>
                <th>Название</th>
                <th>Актив.</th>
              </tr>
              </thead>
              <tbody>
                {coins.map(({ hash, title, active}) =>
                  <tr key={hash}>
                    <td>{title}</td>
                    <td>
                      <CheckBox
                        onChange={() => toggleCheckCoin(hash, active)}
                        name={`checkbox-${hash}`}
                        value={active}
                      />
                    </td>
                  </tr>
                )}
              </tbody>
            </Table>}
        </CardBody>
      </Card>
    </Col>
  )

}
