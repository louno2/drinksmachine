const validate = (values) => {
  const errors = {};
  if (!values.title) {
    errors.title = 'Поле название не должно быть пустым';
  }
  if (values.cost === '') {
    errors.cost = 'Поле стоимость не должно быть пустым';
  }
  if (values.quantity === '') {
    errors.quantity = 'Поле количество не должно быть пустым';
  }

  return errors;
}

export default validate;
