import React, { useState, useEffect } from 'react';
import isEmpty from 'lodash/isEmpty';
import { useDispatch, useSelector } from 'react-redux';
import {
  Col, Card, ButtonToolbar,
  CardBody, Button
} from 'reactstrap';

import validate from './validate';
import TextField from '../../../Common/TextField';
import {
  attemptModifyDrink, attemptClearDrink
} from '../../../../store/thunks/drink';

export default function ModifyDrink () {

  const dispatch = useDispatch();

  const [
    stateValues,
    setStateValues
  ] = useState({});

  const selectedDrink = useSelector(
    st => st.drink.current
  );

  const [
    stateErrors,
    setStateErrors
  ] = useState({});

  useEffect(() => {
    setStateValues(isEmpty(selectedDrink)
      ? { title: '', cost: 0, quantity: 0 }
      : selectedDrink
    );
  }, [selectedDrink]);

  const handleModifyDrink = (event) => {
    event.preventDefault();
    let errors = validate(stateValues);

    if (isEmpty(errors)) {
      dispatch(attemptModifyDrink(stateValues));
      resetValues();
    }
    setStateErrors(errors);
  };

  const handleChange = (event) => {
    let { name, value } = event.target;
    setStateValues(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const resetValues = () => {
    dispatch(attemptClearDrink(
        stateValues.hash
    ))
  }

  return (
    <Col md={3}>
      <Card>
        <CardBody>
          <div className="card__title">
            <h5 className="bold-text">Данные о напитке</h5>
            <h5 className="subhead">добавление / редактирование</h5>
          </div>
          {!isEmpty(stateValues)
           ? <form className="form form--horizontal"
                onSubmit={handleModifyDrink}>
            <TextField
              label="Название"
              name="title"
              value={stateValues.title}
              error={stateErrors.title}
              type="text"
              onChange={handleChange}/>
            <TextField
              label="Стоимость (руб.)"
              name="cost"
              value={stateValues.cost}
              error={stateErrors.cost}
              type="number"
              onChange={handleChange}/>
            <TextField
              label="Количество"
              name="quantity"
              value={stateValues.quantity}
              error={stateErrors.quantity}
              type="number"
              onChange={handleChange}/>
            <ButtonToolbar className="bottom__position">
              <Button type="submit" size="sm" color="success">
                Сохранить
              </Button>
              { stateValues.hasOwnProperty('hash') ?
              <Button onClick={resetValues} type="button" size="sm" color="primary">
                Новый напиток
              </Button> : null }
            </ButtonToolbar>
          </form> : null}
        </CardBody>
      </Card>
    </Col>
  )
}
