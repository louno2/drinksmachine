import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux'
import {
  Col, Card, CardBody, Table
} from 'reactstrap';

import Alert from '../../shared/components/Alert';
import PublicPart from './parts/PublicPart';
import PrivacyPart from './parts/PrivacyPart';

import { rubleSymbol } from '../../utils/constants';
import { attemptSelectDrink, attemptDeleteDrink } from '../../store/thunks/drink';

export default function DrinksList({ isPrivacy }) {

    const dispatch = useDispatch();

    const drinks = useSelector(
      st => st.drink.list
    );

    const selectDrink = (hash) => {
      dispatch(attemptSelectDrink(hash));
    };

    const deleteDrink = (hash) => {
      dispatch(attemptDeleteDrink(hash));
    };

    return (
      <Col md={6}>
        <Card>
          <CardBody>
            <div className="card__title">
              <h5 className="bold-text">Список напитков</h5>
            </div>
            {!drinks.length ?
            <Alert color="info" className="alert--bordered" icon>
              <p> <span className="bold-text">Подсказка: </span>
                Не создано ни одного напитка.
              </p>
            </Alert> :
            <Table className="table--bordered">
              <thead>
              <tr>
                <th>#</th>
                <th>Название</th>
                <th>Стоимость</th>
                {isPrivacy ?<>
                  <th>Кол-во</th>
                  <th>Дата создания</th>
                </> : <>
                  <th>Статус</th>
                </>}
                <th/>
              </tr>
              </thead>
              <tbody>
                {drinks.map(({ hash, title, cost, quantity, checked = false, createdAt }, index) => {
                  return (<tr key={hash}>
                    <td>{index + 1}</td>
                    <td>{title}</td>
                    <td dangerouslySetInnerHTML={{
                      __html: `${cost} ${rubleSymbol}`
                    }}/>
                    {isPrivacy
                      ? <PrivacyPart {...{
                        quantity, hash, createdAt,
                        selectDrink, deleteDrink
                      }} />
                      : <PublicPart {...{
                        quantity, cost,
                        hash, checked
                      }}/>
                    }
                  </tr>)
                })}
              </tbody>
            </Table>}
          </CardBody>
        </Card>
      </Col>
    );
}

DrinksList.propTypes = {
  isPrivacy: PropTypes.bool.isRequired
}
