import React from 'react';
import PropTypes from 'prop-types';
import {
  UncontrolledDropdown, DropdownMenu,
  DropdownToggle, DropdownItem
} from 'reactstrap';
import DotsHorizontalIcon from 'mdi-react/DotsHorizontalIcon';

export default function EditableCell({ hash, deleteDrink, selectDrink }) {
  return (
    <UncontrolledDropdown className="dashboard__table-more">
      <DropdownToggle>
        <p><DotsHorizontalIcon /></p>
      </DropdownToggle>
      <DropdownMenu className="dropdown__menu">
        <DropdownItem onClick={() => selectDrink(hash)}>Ред.</DropdownItem>
        <DropdownItem onClick={() => deleteDrink(hash)}>Удалить</DropdownItem>
      </DropdownMenu>
    </UncontrolledDropdown>
  )
}

EditableCell.propTypes = {
  hash: PropTypes.string.isRequired,
  deleteDrink: PropTypes.func.isRequired,
  selectDrink: PropTypes.func.isRequired
}
