import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import EditableCell from '../EditableCell';

export default function PrivacyPart({
 quantity, createdAt, hash,
 selectDrink, deleteDrink
}) {
  return (<>
    <td>{quantity}</td>
    <td>{moment(createdAt).format('DD.MM.YYYY')}</td>
    <td className="editable-cell">
      <EditableCell {...{
        hash,
        selectDrink,
        deleteDrink
      }}/>
    </td>
  </>)
}

PrivacyPart.propTypes = {
  quantity: PropTypes.number.isRequired,
  hash: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  selectDrink: PropTypes.func.isRequired,
  deleteDrink: PropTypes.func.isRequired
};
