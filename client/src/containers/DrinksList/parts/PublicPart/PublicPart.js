import React from 'react';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import {  Badge } from 'reactstrap';
import CheckBox from '../../../Common/CheckBox';

import { drinkLabels } from '../../../../utils/constants';
import { attemptToggleCheckDrink } from '../../../../store/thunks/drink';
import {
  attemptIncAmount, attemptChangeAmount
} from '../../../../store/thunks/coin';

export default function PublicPart({ cost, quantity, hash, checked }) {

  const dispatch = useDispatch();
  const amount = useSelector(st => get(
    st.coin, 'order.amount', 0
  ));
  const isAvailable = quantity > 0;

  const toggleCheckDrink = () => {
    dispatch(attemptToggleCheckDrink(hash));
    dispatch(attemptChangeAmount(
      checked ? 'inc' :'dec', cost
    ));
  };

  let [ label, color ] =
    drinkLabels[
      isAvailable
        ? 'available'
        : 'notAvailable'
      ].split('|');

  return (<>
    <td>
      <Badge color={color}>{label}</Badge>
    </td>
    <td>
      <CheckBox
        onChange={toggleCheckDrink}
        name={`checkbox-${hash}`}
        value={checked}
        disabled={(!isAvailable || amount < cost) && !checked}
      />
    </td>
  </>);
}

PublicPart.propTypes = {
  cost: PropTypes.number.isRequired,
  quantity: PropTypes.number.isRequired,
  hash: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired
};
