import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  Col, Card, CardBody,
  ButtonToolbar, Button
} from 'reactstrap'

import { rubleSymbol } from '../../../../utils/constants'
import {
  attemptChangeAmount,
  attemptClearCoinData
} from '../../../../store/thunks/coin';

import {
  attemptCompleteOrder
} from '../../../../store/thunks/drink';

import { attemptUncheckDrinks } from '../../../../store/thunks/drink';

export default function CoinsSection () {

  const dispatch = useDispatch()

  const {
    list: coins,
    order: {amount}
  } = useSelector(
    st => st.coin
  );

  const drinks = useSelector(
    st => st.drink.list
  );

  const incAmount = (value) => {
    dispatch(attemptChangeAmount(
      'inc', value
    ));
  }

  const clearData = () => {
    dispatch(attemptClearCoinData());
    dispatch(attemptUncheckDrinks());
  }

  const completeOrder = () => {
    let orderedDrinks = drinks
      .filter(({ checked }) => checked)

    if (orderedDrinks.length) {
      clearData();
      dispatch(attemptCompleteOrder({
        orderedDrinks, amount
      }));
    }
  }

  return (
    <Col md={6}>
      <Card>
        <CardBody>
          <div className="card__title">
            <h5 className="bold-text">Панель с кнопками</h5>
          </div>
          <ButtonToolbar className="center__position">
            {coins.map(({hash, faceValue, active}) =>
              <Button key={hash} color='primary'
                      disabled={!active}
                      onClick={() => incAmount(faceValue)}
                      dangerouslySetInnerHTML={{
                        __html: `${faceValue} ${rubleSymbol}`
                      }}/>
            )}
          </ButtonToolbar>
          <h5 className="cart__total" dangerouslySetInnerHTML={{
            __html: `Сумма: ${amount} ${rubleSymbol}`
          }}/>
          <ButtonToolbar className="form__button-toolbar">
            <Button
              color="success"
              onClick={completeOrder}
              size="sm"
              type="button">
              Подтведить
            </Button>
            <Button
              color="primary"
              onClick={clearData}
              size="sm"
              type="button">
              Сбросить
            </Button>
          </ButtonToolbar>
        </CardBody>
      </Card>
    </Col>
  )
}
