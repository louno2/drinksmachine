import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Container, Row, Button, Col } from 'reactstrap';
import DrinksList from '../DrinksList';

import { attemptGetDrinks } from '../../store/thunks/drink';
import { attemptGetCoins } from '../../store/thunks/coin';

import CoinsSection from './parts/CoinsSection';

export default function DrinksPanel () {

  const dispatch = useDispatch()

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    Promise.all([
      dispatch(attemptGetDrinks('public')),
      dispatch(attemptGetCoins())
    ])
      .then(() => setLoading(false))
  }, [])

  return !loading && (<Container className="dashboard">
      <Link to="/admin?secretKey=abc123">
        <Button>
          admin
        </Button>
      </Link>
      <Row>
        <Col md={12}>
          <h3 className="page-title">Публичная страница</h3>
        </Col>
      </Row>
      <Row>
        <CoinsSection/>
      </Row>
      <Row>
        <DrinksList isPrivacy={false}/>
      </Row>
    </Container>
  )
}
