import React from 'react';
import PropTypes from 'prop-types';
import CheckIcon from 'mdi-react/CheckIcon';

export default function CheckBox({
   name, onChange, value, label, disabled
}) {
  return (
    <label
      className={`checkbox-btn ${disabled ? 'disabled' : ''} checkbox-btn--colored-click`}
      htmlFor={name}
    >
      <input
        className="checkbox-btn__checkbox"
        type="checkbox"
        id={name}
        name={name}
        onChange={onChange}
        checked={value}
        disabled={disabled}
      />
      <span className="checkbox-btn__checkbox-custom">
          <CheckIcon />
      </span>
      <span className="checkbox-btn__label">
          {label}
        </span>
    </label>
  )
};

CheckBox.propTypes = {
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.bool.isRequired,
  label: PropTypes.string,
  disabled: PropTypes.bool
}



