import React from 'react';
import NotificationSystem from 'rc-notification';
import {
    BasicNotification
} from '../../shared/components/Notification';

let notification = null;
NotificationSystem.newInstance({style: { top: 65 }}, n => notification = n);
const titles = {
    success: 'Успешно',
    warning: 'Предупреждение',
    danger: 'Ошибка',
};

export default function showNotification({message, type}) {

    notification.notice({
        content: <BasicNotification
            color={type}
            title={titles[type]}
            message={message}
        />,
        duration: 20,
        closable: true,
        style: {top: -40, left: 'calc(100vw - 100%)'},
        className: `right-up ltr-support`,
    });
};
