import React from 'react';
import PropTypes from 'prop-types';

export default function TextFieldGroup({
    type, name, error = '',
    placeholder, label,
    value, onChange,
}) {
    return (
        <div className="form__form-group">
            {label.length ? (<span className="form__form-group-label">{label}</span>) : null}
            <div className="form__form-group-field">
                <div className="form__form-group-input-wrap form__form-group-input-wrap--error-above">
                    <input
                        placeholder={placeholder}
                        name={name}
                        type={type}
                        value={value}
                        onChange={onChange}
                    />
                    {error && <span className="form__form-group-error">{error}</span>}
                </div>
            </div>
        </div>
    );
}

TextFieldGroup.propTypes = {
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    error: PropTypes.string,
    type: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
};

TextFieldGroup.defaultProps = {
    type: 'text'
};



