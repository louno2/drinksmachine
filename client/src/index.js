import React from 'react';
import 'core-js/stable';
import 'regenerator-runtime/runtime';
import { render } from 'react-dom';
import Root from './containers/App/Root';

import history from './history';
import store from './store';

render(
    <Root history={history} store={store} />,
    document.getElementById('root'),
);


if (module.hot) {
  module.hot.accept();
}
