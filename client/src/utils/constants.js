const rubleSymbol = '&#8381';
const authErr = {
    status: 401,
    text: 'Unauthorized'
}
const drinkLabels = {
  available: 'доступно|success',
  notAvailable: 'нет в наличии|warning'
}

export {
  rubleSymbol,
  drinkLabels,
  authErr
}
