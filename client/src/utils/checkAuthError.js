import { authErr } from './constants';

export default callback => err => {
  if (err.status === authError.status &&
    err.statusText === authError.text) {
    callback(false)
  }
}
