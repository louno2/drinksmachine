import axios from 'axios/index'

export default (paramName, callback = () => {}) => {

  const queryString = window.location.search;
  let paramsSet = new URLSearchParams(queryString);
  let paramValue = paramsSet.get(paramName);

  if (paramValue) {
    axios.defaults.headers.common['Authorization'] = paramValue;
    callback(true)
  } else {
    delete axios.defaults.headers.common['Authorization'];
    callback(false)
  }
}
