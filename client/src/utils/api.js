import get from 'lodash/get';
import showNotification from '../containers/Common/Notification';

export const handleSuccess = resp => {

    const { message } = resp.data;
    if (message) {
        showNotification({
          type: 'success',
          message
        });
    }
    return resp.data;
};

export const handleError = error => {
    if (error.response) {
        throw error.response;
    } else {
        const response = { status: 500, body: { message: 'Внутренняя ошибка сервера' } };
        throw response;
    }
};

export const dispatchError = dispatch => res => {
    let message = get(res, 'data.message', '');
    showNotification({ type: 'danger', message});
    throw res;
};
