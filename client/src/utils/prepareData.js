export default (list, action, callback) => {

  const index = list
    .map(({hash}) => hash)
    .indexOf(action.hash)
  const updatedAtIndex = {
    $splice: [[
      index, 1, callback(list[index], action)
    ]]
  };

  return { index, updatedAtIndex }
};
