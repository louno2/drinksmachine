export const SET_DRINK_LIST = 'SET_DRINK_LIST';
export const ADD_DRINK = 'ADD_DRINK';
export const UPDATE_DRINK = 'UPDATE_DRINK';
export const SELECT_DRINK = 'SELECT_DRINK';
export const CLEAR_DRINK = 'CLEAR_DRINK';
export const TOGGLE_CHECK_DRINK = 'TOGGLE_CHECK_DRINK';
export const UNCHECK_DRINKS = 'UNCHECK_DRINKS';
export const REMOVE_DRINK = 'REMOVE_DRINK';

export const setDrinkList = drinks => {
  return {
    type: SET_DRINK_LIST,
    drinks,
  }
};

export const modifyDrink = ({hash, title, cost, quantity}, isNew) => {
  return {
    type: isNew ? ADD_DRINK : UPDATE_DRINK,
    hash, title,
    cost, quantity
  }
};

export const selectDrink = (hash) => {
  return {
    type: SELECT_DRINK,
    hash
  }
};

export const clearDrink = (hash) => {
  return {
    type: CLEAR_DRINK,
    hash
  }
};

export const uncheckDrinks = () => {
  return {
    type: UNCHECK_DRINKS
  }
};

export const toggleCheckDrink = (hash) => {
  return {
    type: TOGGLE_CHECK_DRINK,
    hash
  }
};

export const removeDrink = hash => ({
  type: REMOVE_DRINK,
  hash,
});
