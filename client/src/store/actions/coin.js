export const SET_COINS = 'SET_COINS';
export const CHANGE_AMOUNT = 'CHANGE_AMOUNT';
export const CLEAR_COIN_DATA = 'CLEAR_COIN_DATA';
export const TOGGLE_LOCK_COIN = 'TOGGLE_LOCK_COIN';

export const setCoins = coins => ({
  type: SET_COINS,
  coins,
});

export const changeAmount = (opt, value) => ({
  type: CHANGE_AMOUNT,
  opt, value,
});

export const clearCoinData = () => ({
  type: CLEAR_COIN_DATA,
});

export const toggleLockCoin = (hash) => ({
  type: TOGGLE_LOCK_COIN,
  hash
});
