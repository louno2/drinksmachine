import { dispatchError } from '../../utils/api';
import { getCoins, putCoin } from '../../api/coin';
import {
  setCoins, changeAmount,
  clearCoinData, toggleLockCoin
} from '../actions/coin';

export const attemptGetCoins = () => dispatch =>
  getCoins()
    .then(({ coins }) => dispatch(setCoins(coins)))
    .catch(dispatchError(dispatch));

export const attemptChangeAmount = (opt, value) => dispatch => {
  dispatch(changeAmount(opt, value));
};

export const attemptToggleLockCoin = (hash, value) => dispatch =>
  putCoin(hash, {active: !value})
    .then(() => dispatch(toggleLockCoin(hash)))
    .catch(dispatchError(dispatch));

export const attemptClearCoinData = () => dispatch => {
  dispatch(clearCoinData());
};
