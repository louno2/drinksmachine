import {
  getDrinks, postDrink,
  completeOrder, deleteOrder
} from '../../api/drink';
import {
  setDrinkList, modifyDrink,
  selectDrink, clearDrink,
  toggleCheckDrink, uncheckDrinks,
  removeDrink
} from '../actions/drink';
import { dispatchError } from '../../utils/api';

export const attemptModifyDrink = data => dispatch => {
  let isNew = !data.hasOwnProperty('hash');
  postDrink(data)
    .then(({ drink }) => dispatch(
      modifyDrink(drink, isNew)
    ))
    .catch(dispatchError(dispatch));
}

export const attemptGetDrinks = (privacyType) => dispatch =>
  getDrinks(privacyType)
    .then(({ drinks }) => dispatch(setDrinkList(drinks)))
    .catch(dispatchError(dispatch));

export const attemptSelectDrink = (hash) => dispatch => {
  dispatch(selectDrink(hash))
};

export const attemptClearDrink = (hash) => dispatch => {
  dispatch(clearDrink(hash))
};

export const attemptToggleCheckDrink = (hash) => dispatch => {
  dispatch(toggleCheckDrink(hash))
};

export const attemptUncheckDrinks = () => dispatch => {
  dispatch(uncheckDrinks())
}

export const attemptCompleteOrder = (data) => dispatch =>
  completeOrder(data)
    .catch(dispatchError(dispatch));

export const attemptDeleteDrink = (hash) => dispatch =>
  deleteOrder(hash)
    .then(() => dispatch(removeDrink(hash)))
    .catch(dispatchError(dispatch));
