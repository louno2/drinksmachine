import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import drink from './drink';
import coin from './coin';

const createRootReducer = history => combineReducers({
    router: connectRouter(history),
    drink, coin
});

export default createRootReducer;
