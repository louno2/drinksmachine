import update from 'immutability-helper';
import prepareData from '../../utils/prepareData';

import {
  SET_DRINK_LIST,
  ADD_DRINK,
  UPDATE_DRINK,
  SELECT_DRINK,
  CLEAR_DRINK,
  TOGGLE_CHECK_DRINK,
  UNCHECK_DRINKS,
  REMOVE_DRINK
} from '../actions/drink'

function drink (state = {}, action) {
  switch (action.type) {
    case ADD_DRINK:
      return update(state, {
        hash: {$set: action.hash},
        title: {$set: action.title},
        cost: {$set: action.cost},
        quantity: {$set: action.quantity},
        checked: {$set: false}
      });
    case UPDATE_DRINK:
      return update(state, {
        title: {$set: action.title},
        cost: {$set: action.cost},
        quantity: {$set: action.quantity}
      });
    case TOGGLE_CHECK_DRINK:
      const prev = state.checked
      return update(state, {
        checked: { $set: !prev },
        quantity: { $apply: value => prev ? value + 1 : value - 1 }
      });
    default:
      return state;
  }
}

export default function drinks (state = {list: [], current: {}}, action) {

  const {
    index, updatedAtIndex
  } = prepareData(
    state.list, action, drink
  );

  switch (action.type) {
    case SET_DRINK_LIST:
      return update(state, {list: {$set: action.drinks}});
    case ADD_DRINK:
      return update(state, {
        list: {
          $push: [
            drink(undefined, action)
          ]
        }
      });
    case UPDATE_DRINK:
    case TOGGLE_CHECK_DRINK:
      return update(state, {list: updatedAtIndex});
    case SELECT_DRINK:
      return update(state, {
        current: {$set: state.list[index]}
      });
    case CLEAR_DRINK:
      return update(state, {current: {$set: {}}});
    case UNCHECK_DRINKS:
      return update(state, { list: {$set: state.list.map(
          (drink) => Object.assign(drink, { checked: false })
        )}});
    case REMOVE_DRINK:
      debugger
      return update(state, {list: { $splice: [[index, 1]] }});
    default:
      return state;
  }
};
