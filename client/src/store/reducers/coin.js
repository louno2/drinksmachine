import update from 'immutability-helper';
import {
  SET_COINS,
  CLEAR_COIN_DATA,
  CHANGE_AMOUNT,
  TOGGLE_LOCK_COIN
} from '../actions/coin';
import prepareData from '../../utils/prepareData';

function coin(state = {}, action) {
  switch (action.type) {
    case TOGGLE_LOCK_COIN:
      return update(state, {
        active: { $apply: x => !x },
      });
    default:
      return state
  }
}

const initialState = {
  list: [],
  order: {
    amount: 0
  }
}

export default function coins(state = initialState, action) {

  const {
    index, updatedAtIndex
  } = prepareData(
    state.list, action, coin
  );

  switch (action.type) {
    case SET_COINS:
      return update(state, {list: {$set: action.coins}});
    case CHANGE_AMOUNT:
      return update(state, { order: {
        amount: { $apply: v => action.opt === 'inc'
            ? v + action.value
            : v - action.value
        }
      }});
    case TOGGLE_LOCK_COIN:
      return update(state, {list: updatedAtIndex});
    case CLEAR_COIN_DATA:
      return update(state, { order: {
       $set: { amount: 0 }
      }})
    default:
      return state;
  }
}
