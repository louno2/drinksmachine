const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const SRC_DIR = __dirname + '/src';
const DIST_DIR = __dirname + '/dist';

module.exports = (env = {}, argv) => {


    const envKeys = Object.keys(env).reduce((prev, next) => {
        prev[`process.env.${next}`] = JSON.stringify(env[next]);
        return prev;
    }, {});


    return {
        entry: [
            SRC_DIR + '/index.js'
        ],
        output: {
            path: DIST_DIR,
            publicPath: '/',
            filename: 'bundle.js'
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader'
                    }
                },
                {
                    test: /\.(scss|sass|css)$/,
                    exclude: /node_modules/,
                    loaders: [
                        MiniCssExtractPlugin.loader,
                        'css-loader',
                        'sass-loader'
                    ]
                },
                {
                    test: /\.(html)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'html-loader',
                        options: {minimize: true}
                    }
                },
                {
                    test: /\.(jpg|jpeg|gif|png|ico|ttf|otf|eot|svg|woff|woff2)(\?[a-z0-9]+)?$/,
                    loader: 'file-loader?name=[path][name].[ext]'
                }
            ]
        },
        resolve: {
            extensions: ['*', '.js', '.jsx']
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new HtmlWebpackPlugin({
                template: SRC_DIR + '/index.html',
                filename: './index.html',
            }),
            new MiniCssExtractPlugin({
                filename: "css/[name].css"
            }),
            new webpack.DefinePlugin(envKeys)
        ],
        devtool: argv.mode === 'development' ? 'source-map' : false,
        performance: {
            hints: false
        },
        devServer: {
            contentBase: DIST_DIR,
            historyApiFallback: true,
            hot: true,
            port: 9000
        }
    };
};
