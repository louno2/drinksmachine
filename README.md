# Автомат с напитками
MongoDB Koa.js React.js Node.js

#### Setup

```bash
yarn install
cd client
yarn install
```

#### config
create default.json from default.example.json
#### for Development
Start the server
```bash
npm start
```
Start the client
```bash
cd client
yarn start
```
#### for Production

```bash
cd client
yarn build
```
Set path to dist into default.json
```bash
 "dist_path": "../client/dist",
cd ..
yarn prod
```
